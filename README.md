Agent Dash
==========

## Cloning and setting up dev environment
Download and install [Vagrant][vagrant] and [Virtualbox][virtualbox]

Run `git clone https://github.com/joeinnes/agent-dash.git`

Change into the `agent-dash` directory

Run `vagrant up`, allow the [Scotch Box][scotch-box] to download and install, and the server to launch (this may take a few minutes).

The site will be available at http://192.168.33.10/ when the Vagrant box is fully up. You can optionally add this to your hosts file as "dev.local", or any other easier-to-remember web address.

## Using the dashboard
The dashboard will make several API calls to ServiceNow.

1. Agent's currently open tickets
2. Agent's recently closed/resolved tickets
3. Recent critical/high incidents

This data will be returned in JSON format, and parsed into a number of locations:

1. The agent welcome panel top left will contain information about the agent's currently open tickets, and their recently closed/resolved tickets
2. The central top panel will contain information about recent critical/high tickets
3. The right "to do" panel will contain information about the agent's currently open tickets

Additional information is required for the dashboard to be fully functional. In particular:

1. Complete/incomplete status of tasks in the to-do panel (v1.2.0)
2. High priority alerts (v1.0.0)
3. Low priority notifications (v1.0.0)

Suggested solutions for this:

1. Local storage, expiring approx once per day.
2. A separate API running on the server, serving up both alerts and notifications from a database. Client-side handling of mixing this in with the ServiceNow data would be preferable (ie: client calls both APIs for data, then sorts the information chronologically as required). A submission interface will be required for this.

[vagrant]: https://www.vagrantup.com/downloads.html "Vagrant"
[virtualbox]: https://www.virtualbox.org/wiki/Downloads "Virtualbox"
[scotch-box]: http://box.scotch.io/ "Scotch Box"