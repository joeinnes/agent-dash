'use strict';

angular.module('agentDashApp',[])

  .controller('CurrentTickets', function ($scope) {
    // Object lists current status of agent's tickets
    $scope.agent = {};
    $scope.tickets = {
      assigned: 1,
      wip: 2,
      pending: 3
    };
    $scope.agent.name = "Joe";
  })

  .controller('TicketStats', function ($scope) {
    // Object lists stats for agent's tickets
    $scope.tickets = {
      hour: 5,
      day: 25,
      week: 400
    };
  })

  .controller('AlertTicker', function ($scope) {
    // Object lists alerts
    $scope.alerts = [{
        type: 'critical',
        location: 'Emerald City',
        text: 'Network down',
        inc: 'INC8675309',
        raisedBy: 'Joe Innes'
      },
      {
        type: 'normal',
        text: 'Do your timesheets!',
        raisedBy: 'Project lead'
      },
      {
        type: 'critical',
        location: 'Gotham City',
        text: 'Can\'t print from Wifi',
        inc: 'INC123456',
        raisedBy: 'Agent X'
      },
      {
        type: 'normal',
        text: 'Christmas party on 5th December'
      },
      {
        type: 'critical',
        location: 'Metropolis',
        text: 'Kryptonite in the server room',
        inc: 'INC123456',
        raisedBy: 'Lex Luthor'
      }];
  })

  .controller('UpdateTicker', function ($scope) {
    // Object lists chat updates
    $scope.updates = [{
      text: 'Hey, anyone know how to get rid of the Outlook disconnected error message?',
      author: 'Joe Innes',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Process update - lost/stolen/broken mobile in Spain',
      author: 'Knowledge Manager',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Who\'s up for the pub tonight?',
      author: 'Agent Z',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Can\'t log on to VPN from home - knowledge article updated',
      author: 'Knowledge Manager',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Check your tickets, guys!',
      author: 'Queue Manager',
      image: 'http://placehold.it/32x32'
    }, {
      text: 'Morning all',
      author: 'Friendly agent',
      image: 'http://placehold.it/32x32'
    }];
  })

  .controller('ToDo', function ($scope) {
    // Object lists todos
    $scope.todos = [{
      task: 'Contact John about INC012345',
      done: false
    }, {
      task: 'Contact Jenny about INC012346',
      done: false
    }, {
      task: 'Contact Dave about INC012347',
      done: true
    }, {
      task: 'Contact Sam about INC012348',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }, {
      task: 'Contact Emily about INC012349',
      done: false
    }];
  });